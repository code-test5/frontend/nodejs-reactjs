# fe-nodejs-reactjs

### Resource
1. Please use our public API to handle product management, base url is `https://60cb2f6921337e0017e440a0.mockapi.io`
2. The product shcema is:
    * id: `ObjectID`
    * name: `VARCHAR`
    * qty: `INT`
    * picture: `TEXT` with Base64 value
    * expiredAt: `DATE` YYYY-MM-DD
    * isActive: `BOOLEAN`
3. And, this is available endpoints for manage product:
    * Add product: `/POST` `/product`
    * Get products: `/GET` `/product`
    * Get product by id: `/GET` `/product/:id`
    * Remove product by id: `/DELETE` `/product/:id`
    * Update product by id: `/PUT` `/product/:id`
4. HTTP response and request body payload already formated in **JSON** format


### Task
1. Create Web App using `ReactJS` to handle product management
2. State management is MUST! Please using `Redux` or `Mobx` for state management
3. Use this API specification rule:
    * The `id` field cannot be updated once the product created
    * For getting product please using `table` element, `datatable` library is prefered for show the products
    * Filtering product by specific criteria, sorter and pagination product is optional
    * Post a product always send `isActive` value to `true`
    * We prefer deleting product using "soft delete" mechanism, with send `isActive` field to `false` value
4. Update product UI will show big **modal dialog** with `form` element specifications:
    * `id` field should disabled `input` element
    * `name` field is `input` element with type=text
    * `qty` field is  `input` element with type=number
    * `picture` field should rendered as an `img` element with base64 value
    * `expiredAt` field should use `date picker` element
    * `isActive` field should use `checkbox` element
5. Also add delete handler in your app
6. Before call `/PUT` or `/DELETE` APIs, add small **modal dialog** just for user confirmation
7. You can use **MVC/MVVM/MVP** design pattern
8. Make your ui element responsive relative to screen size or device type
9. Write clean and effective code for example: proper project structure, ui element, data binding, exception handling, parameterize config, etc.
10. Push your work to your **GitHub** repository, make sure your repository accessible to public.
